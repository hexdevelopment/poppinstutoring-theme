<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SRK
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

    <script>
      window.markerConfig = {
        destination: '6220ca4bb13ca233ef3dc6e6', 
        source: 'snippet'
      };
    </script>

    <script>
    !function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);
    </script>
        
</head>

<body <?php body_class(); ?>>

<div class="top-header_social">
    <div class="hx-container">
        <div class="inner_container">
            <a class="social" href="#"><i class="fa-brands fa-facebook-f"></i></a>
            <a class="num" href="tel:07882973086"><i class="fa-solid fa-phone"></i></a>    
        </div>
    </div>
</div>

<?php srk_header();?>